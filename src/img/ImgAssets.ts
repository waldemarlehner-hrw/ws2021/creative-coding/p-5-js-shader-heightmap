import defaultHeightmap from "./heightmapper-1642463605177.png"

abstract class ImageAssets {
    public static get defaultHeightmapSrc() {
        return defaultHeightmap;
    }
}

export default ImageAssets;