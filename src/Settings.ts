import ImageAssets from "./img/ImgAssets";

class Settings {


    constructor() {
        this.image = ImageAssets.defaultHeightmapSrc;
    }

    image = "";
    imageBoundsY = {min:0, max: 1};
    imageBoundsX = {min:0, max: 1};

    /// Lines
    rowCount: number = 30;
    rowFidelity: number = 200;

    heightmapWeight: number = 200;

    /// Display
    activeHeight: number = 500;
    activeWidth: number = 1000;

    colorA = {r: 0, g: 0, b: 200}
    colorB = {r: 255, g: 255, b: 255}

    
}

export default Settings;