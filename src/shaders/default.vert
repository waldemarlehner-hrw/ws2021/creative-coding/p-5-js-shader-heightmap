precision lowp float;



attribute vec3 aPosition;

varying vec2 vNormalizedPosition;
varying float vHeightData;

uniform vec2 uResolution;
uniform vec2 uActiveSize;

uniform vec2 uXTexBounds;
uniform vec2 uYTexBounds;

uniform sampler2D uHeightmap;

uniform float uHeightmapWeight;

float calculatePositionalOffset(vec2 normalizedPosition) {
    // Sample the texture by treating the input aPosition as if they are texture coordinates.
    // This can be done because they are passed normalized.
    
    // Before doing so though, limit the image to the provided bounds.
    float correctedX = uXTexBounds.x + (uXTexBounds.y - uXTexBounds.x) * normalizedPosition.x;
    float correctedY = uYTexBounds.x + (uYTexBounds.y - uYTexBounds.x) * (1.0-normalizedPosition.y);
   
   
    // Sample RGB from image and take the average.
    vec4 heightmapValue = texture2D(uHeightmap, vec2(correctedX, correctedY));
    float average = (heightmapValue.r + heightmapValue.g + heightmapValue.b) / 3.0;
    // Average should now be a value between 0 and 1.

    // divide heightmapWeight by Resolution to get Normalized Offset
    float normalizedOffsetFactor = uHeightmapWeight / uResolution.y;

    // multiply by average to return normalized offset
    vHeightData = average;
    return average * normalizedOffsetFactor;
}


void main() {
    vNormalizedPosition = aPosition.xy;
    // Just the input. The active region is mapped to [0-1, 0-1]
    vec2 normalizedActiveCoordinates = aPosition.xy;
    // Convert to NDC of just the active part
    vec2 NdcActivePart = normalizedActiveCoordinates * 2.0 - 1.0;
    // By looking at the Resolution and the Active Size, one can determine how "big" the active Region is
    vec2 sizeToResolutionFraction = uActiveSize / uResolution;
    // Multiply active NDC Coordinates with this Fraction
    vec2 positionAppliedCorrectly = NdcActivePart * sizeToResolutionFraction;
    
    if(normalizedActiveCoordinates.y >= 0.0) {
        float positionalOffset = calculatePositionalOffset(normalizedActiveCoordinates);
        positionAppliedCorrectly.y += positionalOffset;
    }
    
    vec3 positionNDC = vec3((aPosition.x * 2.0)-1.0, (aPosition.y * 2.0)-1.0, aPosition.z);



    gl_Position = vec4(positionAppliedCorrectly, aPosition.z, 1.0);
}