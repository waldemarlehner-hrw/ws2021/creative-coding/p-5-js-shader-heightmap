precision lowp float;

varying vec2 vNormalizedPosition;

uniform vec3 uBelowColor;
uniform vec4 uFillColor;
uniform sampler2D uHeightmap;
varying float vHeightData;

void main() {

    //gl_FragColor = color;
    if(vNormalizedPosition.x >= 0.0 && vNormalizedPosition.x <= 1.0) {
        gl_FragColor = uFillColor / 255.0;
    }
    else {
        gl_FragColor = vec4(vec3(0.0), 1.0);
    }
}
