import p5 from "p5";
import Settings from "../Settings";
import defaultFrag from "./default.frag?raw"
import defaultVert from "./default.vert?raw"

abstract class DefaultShaderFactory {

    public static build(p5: p5, _settings: Settings) {
        return p5.createShader(defaultVert, defaultFrag);
    }
}

export default DefaultShaderFactory;