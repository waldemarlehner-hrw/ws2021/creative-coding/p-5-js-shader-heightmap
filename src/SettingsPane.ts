import { ButtonApi, Pane } from "tweakpane";
import Settings from "./Settings";
//@ts-ignore
import * as TweakPaneImagePlugin from "tweakpane-image-plugin";
import * as TweakpaneEssentials from "@tweakpane/plugin-essentials";

class SettingsPane {
    
    public imageButton: ButtonApi;

	getSetting(): Settings {
        return this.settings;
    }

    private tweakpane;
    fpsGraph: any;

    constructor(private settings: Settings) {
        this.tweakpane = new Pane();
        this.tweakpane.registerPlugin(TweakpaneEssentials)
        //this.tweakpane.registerPlugin(TweakPaneImagePlugin)
        this.fpsGraph = this.tweakpane.addBlade({
            view: "fpsgraph", label: "FPS", lineCount: 2
        })
        const inputFolder = this.tweakpane.addFolder({"title": "Heightmap"})
        inputFolder.addInput(settings, "image", {
        })
        this.imageButton = inputFolder.addButton({title: "Update Image"})
        inputFolder.addInput(settings, "imageBoundsX", {min:0, max: 1})
        inputFolder.addInput(settings, "imageBoundsY", {min:0, max: 1})
        
        inputFolder.addSeparator()
       
        
        const renderFolder = this.tweakpane.addFolder({title: "Rendering/Display"})
        renderFolder.addInput(settings, "rowCount", {min: 0, step: 1})
        renderFolder.addInput(settings, "rowFidelity", {min: 0, step: 1})
        renderFolder.addInput(settings, "heightmapWeight")
        renderFolder.addInput(settings, "colorA");
        renderFolder.addInput(settings, "colorB");

        renderFolder.addInput(settings, "activeHeight", {min:0, step: 1, max: 2000})
        renderFolder.addInput(settings, "activeWidth", {min:0, step: 1, max: 2000})
    }
}

export default SettingsPane;