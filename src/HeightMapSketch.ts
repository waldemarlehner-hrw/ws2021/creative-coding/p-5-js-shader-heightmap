import p5 from "p5";
import Settings from "./Settings";
import SettingsPane from "./SettingsPane";
import DefaultShaderFactory from "./shaders/DefaultShaderFactory";


class HeightMapSketch {
	
	private settings: SettingsPane
	private canvas?: p5.Renderer;
	private shader!: p5.Shader;

	private domElement: HTMLElement
	private heightMapTexture!: p5.Image
	private heightMapTextureLastFrame!: p5.Image;


	constructor(private p: p5){
		// Debug
		window.onkeydown = () => console.info(this);

		this.settings = new SettingsPane(new Settings());
		const domElement = document.getElementById("app");
		if(!domElement) {
			throw new Error("Failed to reference Dom Element")
		}
		this.domElement = domElement;

		p.preload = () => {
			this.heightMapTexture = p.loadImage(this.settings.getSetting().image);
		}
		p.setup = () => this.setup();
		p.draw = () => this.draw();
		p.windowResized = () => this.handleDomElementResize();

		this.settings.imageButton.on("click", () => {
			p.loadImage(this.settings.getSetting().image, (img) => {
				this.heightMapTexture = img;
			})
		})
		
	}

	private handleDomElementResize() {
		this.p.resizeCanvas(this.domElement.clientWidth, this.domElement.clientHeight);
		this.shader.setUniform("uResolution", [this.domElement.clientWidth , this.domElement.clientHeight]);

	}

	private setup() {
		this.canvas = this.p.createCanvas(this.domElement.clientWidth, this.domElement.clientHeight, "webgl");
		this.canvas.parent("app");
		this.p.noStroke();
		this.shader = DefaultShaderFactory.build(this.p, this.settings.getSetting())
		this.p.shader(this.shader);
		this.shader.setUniform("uResolution", [this.domElement.clientWidth , this.domElement.clientHeight]);
	}

	private draw() {
		this.settings.fpsGraph.begin();
		this.p.background(0, 0, 0);
		const settings = this.settings.getSetting();

		/// Set Shader Params
		this.shader.setUniform("uActiveSize", [settings.activeWidth, settings.activeHeight]);
		this.shader.setUniform("uHeightmapWeight", settings.heightmapWeight)
		this.shader.setUniform("uXTexBounds", [settings.imageBoundsX.min, settings.imageBoundsX.max])
		this.shader.setUniform("uYTexBounds", [settings.imageBoundsY.min, settings.imageBoundsY.max])
		this.shader.setUniform("uHeightmapWeight", settings.heightmapWeight)
		this.shader.setUniform("uBelowColor", [settings.colorB.r, settings.colorB.g, settings.colorB.b])
		/// Update Texture if new exists
		if(this.heightMapTexture !== this.heightMapTextureLastFrame) {
			this.heightMapTextureLastFrame = this.heightMapTexture;
			this.shader.setUniform("uHeightmap", this.heightMapTexture);
		}

		
		this.p.rectMode("corners")
		for(let y = settings.rowCount - 1; y >= 0; y--) {
			// For each row, create two shapes. One "filler", and one line
			const yFraction = y/(settings.rowCount-1)

			let color = {
				r: yFraction * settings.colorA.r + (1-yFraction) * settings.colorB.r, 
				g: yFraction * settings.colorA.g + (1-yFraction) * settings.colorB.g, 
				b: yFraction * settings.colorA.b + (1-yFraction) * settings.colorB.b, 
			}

			this.shader.setUniform("uFillColor", [color.r, color.g, color.b, 225])
			this.drawRowFiller(y, settings);
		
		}


		this.settings.fpsGraph.end();
	}
	private drawRowFiller(y: number, settings: Settings) {
		

		const yBaseLine = y/(settings.rowCount-1)
		const xEntryWidth = 1/(settings.rowFidelity)


		for(let x = -1; x <= settings.rowFidelity; x++) {
			this.p.beginShape()
			this.p.vertex(x*xEntryWidth, yBaseLine);
			this.p.vertex((x+1)*xEntryWidth, yBaseLine);
			this.p.vertex((x+1)*xEntryWidth, -0.01);
			this.p.vertex((x)*xEntryWidth, -0.01);
			this.p.endShape("close");
		}

	}
	
	
	public static toP5Sketch(): (p5: p5) => void {
		return (p5: p5) => new HeightMapSketch(p5);
	}
}

export default HeightMapSketch;
